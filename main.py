#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     main
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/9/23
-------------------------------------------------
   修改描述-2021/9/23:
-------------------------------------------------
"""

from apps import create_app
app =create_app()


if __name__ == '__main__':
    # 启动服务
    from apps.utils.routes_helper import print_all_routes_info
    print_all_routes_info(app)

    # 上面这种方式无法查询到使用ApiROUD进来的路由
    # ,log_level=1000这个配置关闭日志的无效
    #  # log_level="info",
    # # log_config=LOGGING_CONFIG,
    import uvicorn

    uvicorn.run('main:app',host='127.0.0.1', port=9080, debug=False, reload=True, access_log=False,workers=1, use_colors=True)
    # uvicorn.run('main:app', host='127.0.0.1', port=9080)