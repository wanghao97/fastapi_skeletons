#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     __init__.py
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/9/23
-------------------------------------------------
   修改描述-2021/9/23:         
-------------------------------------------------
"""
# 统一的加载所有的模块的路由信息
def init_routes(app):
    from apps.utils.routes_helper import  register_nestable_blueprint_for_log
    register_nestable_blueprint_for_log(app, project_name='apps', api_name='modules.hanxuan', key_attribute='bp')