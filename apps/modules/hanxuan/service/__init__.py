#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     __init__.py
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/7/15
-------------------------------------------------
   修改描述-2021/7/15:         
-------------------------------------------------
"""

from ..models.yuyue_model import *
from apps.utils.json_helper import class_to_dict
from apps.modules.hanxuan.utils import datatime_help
import collections
import time
import datetime


class LogicService():

    @classmethod
    def get_doctor_info_list(cls):
        with session_scope():
            _result = Doctor.select(Doctor.pic,
                                    Doctor.dno,
                                    Doctor.dnname,
                                    Doctor.rank,
                                    Doctor.fee,
                                    Doctor.destag).dicts().order_by(Doctor.ctime)

            if not _result:
                # 把对应的结果保存到缓存中---缓存穿透：处理
                return False, None  # 查询没有结果的时候

            # 把对应的结果保存到缓存中-时间超时 60*60=一个小时 --10天后过期
            return True, [v for v in _result]

    @classmethod
    def get_doctor_info(cls, dno):
        with session_scope():
            try:
                _result = Doctor.select(
                    Doctor.dnname,
                    Doctor.pic,
                    Doctor.dno,
                    Doctor.rank,
                    Doctor.fee,
                    Doctor.destag,
                    Doctor.describe
                ).where(Doctor.dno == dno).get()
            except:
                _result = None

            if not _result:
                # 把对应的结果保存到缓存中---缓存穿透：处理
                return False, None  # 查询没有结果的时候

            # 把对应的结果保存到缓存中-时间超时 60*60=一个小时 --10天后过期
            return True, class_to_dict(_result)

    @classmethod
    def get_doctor_info_order(cls, dno):
        with session_scope():

            try:
                _result = Doctor.select(
                    Doctor.dnname,
                    Doctor.pic,
                    Doctor.dno,
                    Doctor.rank,
                    Doctor.fee,
                    Doctor.addr
                ).where(Doctor.dno == dno).get()
            except:
                _result = None

            if not _result:
                # 把对应的结果保存到缓存中---缓存穿透：处理
                return False, None  # 查询没有结果的时候

            # 把对应的结果保存到缓存中-时间超时 60*60=一个小时 --10天后过期
            return True, class_to_dict(_result)

    @classmethod
    def get_doctor_nsnum_info_order(cls, dno, nsindex):
        with session_scope():
            try:
                _result = DoctorNsnumInfo.select(
                    DoctorNsnumInfo.dnotime,
                    DoctorNsnumInfo.ampm,
                    DoctorNsnumInfo.tiempm,
                ).where(DoctorNsnumInfo.dno == dno, DoctorNsnumInfo.nsindex == nsindex).get()
            except:
                _result = None

            if not _result:
                # 把对应的结果保存到缓存中---缓存穿透：处理
                return False, None  # 查询没有结果的时候

            # 把对应的结果保存到缓存中-时间超时 60*60=一个小时 --10天后过期
            return True, class_to_dict(_result)

    @classmethod
    def get_doctor_scheduling_info_tiempm_check(cls, dno, nsindex):
        with session_scope():
            try:
                _result = DoctorNsnumInfo.select(DoctorNsnumInfo.nsnum, DoctorNsnumInfo.tiempm).where(DoctorNsnumInfo.dno == dno, DoctorNsnumInfo.nsindex == nsindex).get()
            except:
                return False, None  # 查询没有结果的时候
            if not _result:
                # 把对应的结果保存到缓存中---缓存穿透：处理
                return False, None  # 查询没有结果的时候

            # 开始对比时间和预约号源数
            today = time.strftime('%Y-%m-%d %H:%M', time.localtime(time.time())).replace("-", "")
            todaydate = datetime.datetime.strptime(today, "%Y%m%d %H:%M")  # 字符串转化为date形式
            # 预约时段的时间
            yuyue_day = _result.tiempm.replace("-", "")
            yuyue_daydate = datetime.datetime.strptime(yuyue_day, "%Y%m%d %H:%M:00")  # 字符串转化为date形式
            if todaydate >= yuyue_daydate:
                return False, None  # 查询没有结果的时候
            # 把对应的结果保存到缓存中-时间超时 60*60=一个小时 --10天后过期
            return True, True if _result.nsnum > 0 else False

    def get_doctor_scheduling_info_dnotime_(cls, dno, nsindex):
        with session_scope():
            try:
                _result = DoctorNsnumInfo.select(DoctorNsnumInfo.nsnum, DoctorNsnumInfo.tiempm).where(DoctorNsnumInfo.dno == dno, DoctorNsnumInfo.nsindex == nsindex).get()
            except:
                return False, None  # 查询没有结果的时候
            if not _result:
                # 把对应的结果保存到缓存中---缓存穿透：处理
                return False, None  # 查询没有结果的时候

            # 开始对比时间和预约号源数
            today = time.strftime('%Y-%m-%d %H:%M', time.localtime(time.time())).replace("-", "")
            todaydate = datetime.datetime.strptime(today, "%Y%m%d %H:%M")  # 字符串转化为date形式
            # 预约时段的时间
            yuyue_day = _result.tiempm.replace("-", "")
            yuyue_daydate = datetime.datetime.strptime(yuyue_day, "%Y%m%d %H:%M:00")  # 字符串转化为date形式
            if todaydate >= yuyue_daydate:
                return False, None  # 查询没有结果的时候
            # 把对应的结果保存到缓存中-时间超时 60*60=一个小时 --10天后过期
            return True, True if _result.nsnum > 0 else False

    @classmethod
    def creat_order_info(cls, **kwargs):
        with session_scope():
            pass
            return DoctorNsnumOrder.get_or_create(**kwargs)

    @classmethod
    def updata_order_info_byid(cls, updata={}):
        with session_scope():
            pass
            return DoctorNsnumOrder.update(updata).where(DoctorNsnumOrder.id == id).execute()

    @classmethod
    def get_order_info_list_by_visit_uopenid(cls, visit_uopenid,statue = 4):
        with session_scope():

            _result = DoctorNsnumOrder.select(
                DoctorNsnumOrder.ctime,
                DoctorNsnumOrder.orderid,
                # 订单状态（1:订单就绪，还没支付 2：已支付成功 3：取消订单 4:超时自动取消）
                DoctorNsnumOrder.statue,
                DoctorNsnumOrder.dno,
                DoctorNsnumOrder.doctot_name,
                DoctorNsnumOrder.visitday,
                DoctorNsnumOrder.visittime,
                DoctorNsnumOrder.payfee,
                DoctorNsnumOrder.payactions,
                DoctorNsnumOrder.visit_uname,
                DoctorNsnumOrder.visit_uphone,
                DoctorNsnumOrder.visit_usex,
                DoctorNsnumOrder.visit_uage,
                DoctorNsnumOrder.visit_statue,
                DoctorNsnumOrder.visit_sure_time,
            ).dicts().order_by(DoctorNsnumOrder.ctime).where(DoctorNsnumOrder.visit_uopenid == visit_uopenid,
                                                             DoctorNsnumOrder.statue !=statue,
                                                             )

            if not _result:
                # 把对应的结果保存到缓存中---缓存穿透：处理
                return False, None  # 查询没有结果的时候

            # 把对应的结果保存到缓存中-时间超时 60*60=一个小时 --10天后过期
            return True, [v for v in _result]

    @classmethod
    def updata_order_info_byorder_dno_olny(cls, dno, orderid, updata={}):
        with session_scope():
            return True, DoctorNsnumOrder.update(updata).where(DoctorNsnumOrder.dno == dno, DoctorNsnumOrder.orderid == orderid).execute()

    @classmethod
    def get_order_info_byorder_dno_state(cls, dno, orderid):
        pass
        return DoctorNsnumOrder.get_or_none(DoctorNsnumOrder.dno == dno, DoctorNsnumOrder.orderid == orderid)

    @classmethod
    def get_order_info_byvisit_uopenid_state(cls, visit_uopenid, statue):
        try:
            return DoctorNsnumOrder.select(DoctorNsnumOrder.visit_statue).where(DoctorNsnumOrder.visit_uopenid == visit_uopenid, DoctorNsnumOrder.statue == statue).get()
        except:
            return None


    @classmethod
    def updata_order_info_byorder_dno(cls, dno, orderid, updata={}):
        with session_scope():
            pass
            reustl = DoctorNsnumOrder.get_or_none(DoctorNsnumOrder.dno == dno, DoctorNsnumOrder.orderid == orderid)
            # 订单状态（1:订单就绪，还没支付 2：已支付成功 3：取消订单 4：超时未支付订单）
            if reustl and reustl.statue == 1:
                return True, DoctorNsnumOrder.update(updata).where(DoctorNsnumOrder.dno == dno, DoctorNsnumOrder.orderid == orderid).execute()

            return False, None  # 查询没有结果的时候

    @classmethod
    def creat_visit_user_info(cls, **kwargs):
        with session_scope():
            return VisitUserInfo.get_or_create(**kwargs)

    @classmethod
    def get_visit_user_info(cls, openid):
        with session_scope():
            return VisitUserInfo.get_or_none(VisitUserInfo.openid == openid)
