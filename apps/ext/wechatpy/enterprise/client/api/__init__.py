# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from apps.ext.wechatpy.enterprise.client.api.agent import WeChatAgent  # NOQA
from apps.ext.wechatpy.enterprise.client.api.appchat import WeChatAppChat  # NOQA
from apps.ext.wechatpy.enterprise.client.api.batch import WeChatBatch  # NOQA
from apps.ext.wechatpy.enterprise.client.api.chat import WeChatChat  # NOQA
from apps.ext.wechatpy.enterprise.client.api.department import WeChatDepartment  # NOQA
from apps.ext.wechatpy.enterprise.client.api.jsapi import WeChatJSAPI  # NOQA
from apps.ext.wechatpy.enterprise.client.api.material import WeChatMaterial  # NOQA
from apps.ext.wechatpy.enterprise.client.api.media import WeChatMedia  # NOQA
from apps.ext.wechatpy.enterprise.client.api.menu import WeChatMenu  # NOQA
from apps.ext.wechatpy.enterprise.client.api.message import WeChatMessage  # NOQA
from apps.ext.wechatpy.enterprise.client.api.misc import WeChatMisc  # NOQA
from apps.ext.wechatpy.enterprise.client.api.oauth import WeChatOAuth  # NOQA
from apps.ext.wechatpy.enterprise.client.api.service import WeChatService  # NOQA
from apps.ext.wechatpy.enterprise.client.api.shakearound import WeChatShakeAround  # NOQA
from apps.ext.wechatpy.enterprise.client.api.tag import WeChatTag  # NOQA
from apps.ext.wechatpy.enterprise.client.api.user import WeChatUser  # NOQA
