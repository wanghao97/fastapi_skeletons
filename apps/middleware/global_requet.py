#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     auth
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/6/7
-------------------------------------------------
   修改描述-2021/6/7:         
-------------------------------------------------
"""


from starlette.middleware.base import BaseHTTPMiddleware
from starlette.requests import Request

class GlobalQuestyMiddleware(BaseHTTPMiddleware):


    async def dispatch(self, request: Request, call_next):
        # 给当前的app这是当前的请求上下文对象
        request.app.state.curr_request = request
        response = await call_next(request)
        # 请求之后响应体
        return response
