#!/usr/bin/evn python
# coding=utf-8
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +
#        ┏┓　　　┏┓+ +
# 　　　┏┛┻━━━┛┻┓ + +
# 　　　┃　　　　　　 ┃ 　
# 　　　┃　　　━　　　┃ ++ + + +
# 　　 ████━████ ┃+
# 　　　┃　　　　　　 ┃ +
# 　　　┃　　　┻　　　┃
# 　　　┃　　　　　　 ┃ + +
# 　　　┗━┓　　　┏━┛
# 　　　　　┃　　　┃　　　　　　　　　　　
# 　　　　　┃　　　┃ + + + +
# 　　　　　┃　　　┃　　　　Codes are far away from bugs with the animal protecting　　　
# 　　　　　┃　　　┃ + 　　　　神兽保佑,代码无bug　　
# 　　　　　┃　　　┃
# 　　　　　┃　　　┃　　+　　　　　　　　　
# 　　　　　┃　 　　┗━━━┓ + +
# 　　　　　┃ 　　　　　　　┣┓
# 　　　　　┃ 　　　　　　　┏┛
# 　　　　　┗┓┓┏━┳┓┏┛ + + + +
# 　　　　　　┃┫┫　┃┫┫
# 　　　　　　┗┻┛　┗┻┛+ + + +
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +"""
"""
# 版权说明
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +

# @Time  : 2020/4/21 13:07

# @Author : mayn

# @Project : ZFlask

# @FileName: captcha_helper.py

# @Software: PyCharm

# 作者：小钟同学

# 著作权归作者所有

# 文件功能描述: 图片验证码 不使用第三方插件库的情况下
"""

import random
import string

"""
Image: 画布
ImageDraw: 画笔
ImageFont: 字体
"""
from PIL import Image, ImageDraw, ImageFont

import random
from PIL import Image, ImageDraw, ImageFont
import time
import os
import string


# Captcha验证码
class Captcha():
    # 把常亮抽取成类属性
    # 字体的位置
    font_path = os.path.join(os.path.dirname(__file__), 'simhei.ttf')
    # 生成几位数的验证码
    number = 4
    # 生成验证码的宽度和高度
    size = (100, 40)
    # 背景颜色，默认白色RGB(red,green,blue)
    bgcolor = (0, 0, 0)
    # 随机字体颜色
    random.seed(int(time.time()))
    fontcolor = (random.randint(200, 255), random.randint(100, 255), random.randint(100, 255))
    # 验证码字体大小
    fontsize = 20
    # 随机干扰线颜色
    linecolor = (random.randint(0, 250), random.randint(0, 255), random.randint(0, 250))
    # 是否要加入干扰线
    draw_line = True
    # 是否回执干扰线
    draw_point = True
    # 加入干扰线的条数
    line_number = 3

    SOURCE = list(string.ascii_letters)
    for index in range(0, 10):
        SOURCE.append(str(index))

    # 用来随机生成一个字符串（包括英文和数字）
    # 定义成类方法，然后是私有的，对象在外面不能直接调用
    @classmethod
    def gene_text(cls):
        return "".join(random.sample(cls.SOURCE, cls.number))

    # 用来回执干扰线
    @classmethod
    def __gene_line(cls, draw, width, height):
        begin = (random.randint(0, width), random.randint(0, height))
        end = (random.randint(0, width), random.randint(0, height))
        draw.line([begin, end], fill=cls.linecolor)

    # 用来回执干扰点
    @classmethod
    def __gene_points(cls, draw, point_chance, width, height):
        chance = min(100, max(0, int(point_chance)))
        for w in range(width):
            for h in range(height):
                tmp = random.randint(0, 100)
                if tmp > 100 - chance:
                    draw.point((w, h), fill=(0, 0, 0))

    # 生成验证码
    @classmethod
    def gene_code(cls):
        width, height = cls.size
        image = Image.new('RGBA', (width, height), cls.bgcolor)  # 创建画板
        font = ImageFont.truetype(cls.font_path, cls.fontsize)  # 验证码字体
        draw = ImageDraw.Draw(image)  # 创建画笔
        text = cls.gene_text()  # 生成字符串
        font_width, font_height = font.getsize(text)
        draw.text(((width - font_width) / 2, (height - font_height) / 2), text, font=font, fill=cls.fontcolor)
        if cls.draw_line:
            # 遍历line_number次，就是画line_number根条线
            for x in range(0, cls.line_number):
                cls.__gene_line(draw, width, height)
        # 如果要回执噪点
        if cls.draw_point:
            cls.__gene_points(draw, 10, width, height)
        return (text, image)


if __name__ == '__main__':
    from io import BytesIO
    from flask import Blueprint, request, make_response, session


    # @route_admin.route('/Captcha', methods=['GET'])
    def GetCaptcha():
        text, image = Captcha().gen_graph_captcha()
        out = BytesIO()
        image.save(out, 'png')
        out.seek(0)
        resp = make_response(out.read())
        resp.content_type = 'image/png'
        # 存入session
        # session['Captcha'] = text
        return resp
