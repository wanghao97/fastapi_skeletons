#!/usr/bin/evn python
# coding=utf-8

import os
import sys

### 获取程序当前运行的堆栈信息
def get_detailtrace():
    """
    异常处理工具包，主要是获取当前运行错误的时候，获取堆栈错误信息
    """
    retStr = ""
    f = sys._getframe()
    f = f.f_back
    while hasattr(f, "f_code"):
        co = f.f_code
        retStr = "%s(%s:%s)->" % (os.path.basename(co.co_filename),
                                  co.co_name,
                                  f.f_lineno) + retStr
        f = f.f_back
    return retStr
