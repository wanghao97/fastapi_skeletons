#!/usr/bin/evn python
# coding=utf-8

import json


# jsobj = JsonObject()
# 		if u.isExisted():
# 			jsobj.put("code",1)
# 			jsobj.put("desc","User Existed")
# 		else:
# 			jsobj.put("code",2)
# 			jsobj.put("desc","User Not Existed")
# 		return jsobj.getJson(),200

class JsonObject():
    def __init__(self):
        self.dic = {}

    def put(self, key, value):
        self.dic[key] = value

    def get(self, key):
        return self.dic[key]

    def getJson(self):
        return json.dumps(self.dic, ensure_ascii=False).replace('}"', '}')

    def getDic(self):
        return self.dic


def callbackJsonObject():
    return JsonObject()
